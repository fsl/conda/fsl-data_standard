#!/usr/bin/env bash

set -e

mkdir -p      $PREFIX/data/standard
cp -r *       $PREFIX/data/standard/
chmod -R 0755 $PREFIX/data/standard/*
